<?php
use app\core\Router;

require_once 'loader.php';
require_once 'app/config/config.php';

define('ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);
ini_set('include_path', ROOT);

spl_autoload_register('Loader');

$router = new Router();
$router->run();