
<form action="<?= URL ?>panel/saveproduct" method="POST">
    <p>Product:
        <input type="text" name ="product_name" value="<?= $product->product_name ?>">
        <select name="category_id">
            <?php foreach ($categories as $category): ?>
                <option value="<?= $category->id ?>"> <?= $category->category_name ?> </option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" value="<?= $product->id ?>" name="product_id">
        <input type="text" name ="product_price" value="<?= $product->product_price ?>">
        <input type="submit" name="update_product">
    </p>
</form>

