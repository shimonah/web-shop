<h2>Table Categories</h2>
<form action="<?= URL . 'panel/addproduct' ?>" method="POST">
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Product name</th>
                <th>Products category</th>
                <th>Price</th>
                <th>Control</th>
            </tr>
        </thead>
        <tbody>            
                <?php foreach ($products as $product): ?>
                <tr>
                    <td> <?= $product->id ?> </td>
                    <td> <?= $product->product_name ?> </td>
                    <td> <?= $product->category_name ?> </td>
                    <td> <?= $product->product_price ?> </td>
                    <td>
                        <a href="<?= URL . 'panel/updateproduct/' . $product->id ?>"><img src="/img/update-icon.png"></a>
                        <a href="<?= URL . 'panel/deleteproduct/' . $product->id ?>"><img src="/img/trash-icon.png"></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                
                <tr>
                    <td></td>
                    <td> <input type="text" name ="product_name"> </td>
                    <td> 
                        <select name="category_id">
                            <?php foreach ($categories as $category): ?>
                            <option value="<?= $category->id ?>"> <?= $category->category_name ?> </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td> <input type="text" name ="product_price"> </td>
                    <td> <input type="submit" value="add product" name="add_new_product"> </td>
                </tr>

            
        </tbody>
    </table>
</form>
