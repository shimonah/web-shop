<h2>Table Orders</h2>
    <table>
        <thead>
            <tr>
                <th>ID</th>                
                <th>Product</th>
                <th>User</th>
                <th>Control</th>
            </tr>
        </thead>
        <tbody>
                <?php foreach ($orders as $order): ?>
                <tr>
                    <td> <?= $order->id ?> </td>
                    <td> <?= $order->product_name ?> </td>
                    <td> <?= $order->user_name ?> </td>                    
                    <td>
                        <a href="<?= URL . 'panel/deleteorder/' . $order->id ?>"><img src="/img/trash-icon.png"></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                
            
        </tbody>
    </table>

