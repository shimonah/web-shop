<h2>Table Categories</h2>
<form action="<?= URL . 'panel/addcategory' ?>" method="POST">
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Category name</th>
                <th>Control</th>
            </tr>
        </thead>
        <tbody>            
                <?php foreach ($categories as $category): ?>
                <tr>
                    <td> <?= $category->id ?> </td>
                    <td> <?= $category->category_name ?> </td>                    
                    <td>
                        <a href="<?= URL . 'panel/updatecategory/' . $category->id ?>"><img src="/img/update-icon.png"></a>
                        <a href="<?= URL . 'panel/deletecategory/' . $category->id ?>"> <img src="/img/trash-icon.png"> </a>
                    </td>
                </tr>
                <?php endforeach; ?>
                
                <tr>
                    <td></td>
                    <td> <input type="text" name ="category_name"> </td>
                    <td> <input type="submit" name="add_new_category" value="add category"> </td>
                </tr>

            
        </tbody>
    </table>
</form>