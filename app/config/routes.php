<?php
return array (
    
//    PanelController routes
    
    'panel/deleteorder/([0-9]+)' => 'panel/deleteorder/$1',
    
    'panel/deleteproduct/([0-9]+)' => 'panel/deleteproduct/$1',
    'panel/updateproduct/([0-9]+)' => 'panel/updateproduct/$1',
    'panel/saveproduct' => 'panel/saveproduct',
    'panel/addproduct' => 'panel/addproduct',
    
    'panel/updatecategory/([0-9]+)' => 'panel/updatecategory/$1',
    'panel/deletecategory/([0-9]+)' => 'panel/deletecategory/$1',
    'panel/savecategory' => 'panel/savecategory',
    'panel/addcategory' => 'panel/addcategory',
    
    'panel/categories' => 'panel/showcategories',
    'panel/products' => 'panel/showproducts',
    'panel/orders' => 'panel/showorders',
    'panel/main' => 'panel/main',
    
//    ShopController routes
    'shop/result' => 'shop/result',
    'shop/addorder/([0-9]+)' => 'shop/addorder/$1',
    'shop/order/([0-9]+)' => 'shop/order/$1',
    'shop/category/([0-9]+)/([0-9]+)' => 'shop/showproduct/$1/$2',
    'shop/category/([0-9]+)' => 'shop/showcategory/$1',
    'shop' => 'shop/main',
    '' => 'shop/main',
);