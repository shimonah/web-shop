<?php
namespace app\controllers;

use app\core\Controller;
use app\models\PanelModel;
/**
 * Description of PanelController
 *
 */
class PanelController extends Controller
{
    public $table;

    public function main()
    {
        $this->render('main');
    }

    public function showCategories()
    {
        $this->table = 'categories';

        $model = new PanelModel();
        $categories = $model->getAllFrom($this->table);

        $this->render('table_categories', [
            'categories' => $categories,
        ]);
    }

    public function addCategory()
    {

        if (isset($_POST['add_new_category']))
        {
            $category_name = $this->checkInput($_POST['category_name']);

            if (!empty($category_name))
            {
                $model = new PanelModel();
                $model->addCategory($category_name);
                
                header('Location: ' . URL . 'panel/categories/');
            } else {
                header('Location: ' . URL . 'panel/categories/');
            }
        }

    }

    public function deleteCategory($category_id)
    {
        $this->table = 'categories';

        $model = new PanelModel();
        $model->deleteRow($category_id, $this->table);

        header('Location: ' . URL . 'panel/categories/');

    }

    public function updateCategory($category_id)
    {
        $this->table = 'categories';
        
        $model = new PanelModel();
        $category = $model->getRow($category_id, $this->table);
        
        $this->render('update_categories', [
            'category' => $category,
        ]);
    }
    
    public function saveCategory()
    {
        var_dump($_POST);
        if (isset($_POST['update_category']))
        {
            $category_name = $this->checkInput($_POST['category_name']);
            $category_id = $_POST['category_id'];

            if (!empty($category_name))
            {
                $model = new PanelModel();
                $model->updateCategory($category_name, $category_id);
                
                header('Location: ' . URL . 'panel/categories');
            } else {
                header('Location: ' . URL . 'panel/categories');
            }
        }
    }

    public function showProducts()
    {
        $this->table = 'products';
        
        $model = new PanelModel();
        $products = $model->getAllForProducts();
        $categories = $model->getAllFrom('categories');
        
        $this->render('table_products',[
            'products' => $products,
            'categories' => $categories,
        ]);
    }
    
    public function deleteProduct($product_id)
    {
        $this->table = 'products';
        
        $model = new PanelModel();
        $model->deleteRow($product_id, $this->table);
        
        header('Location: ' . URL . 'panel/products');
    }
    
    public function addProduct()
    {
        if (isset($_POST['add_new_product']))
        {
            $product_name = $this->checkInput($_POST['product_name']);
            $category_id = $this->checkInput($_POST['category_id']);
            $product_price = $this->checkInput($_POST['product_price']);
            
            if (!empty($product_name) && ctype_digit($product_price))
            {
                $model = new PanelModel();
                $model->addProduct($product_name, $category_id, $product_price);
                
                header('Location: ' . URL . 'panel/products');
            } else {
                header('Location: ' . URL . 'panel/products');
            }
        }
    }
    
    public function updateProduct($product_id)
    {
        $this->table = 'products';
        
        $model = new PanelModel();
        $product = $model->getRow($product_id, $this->table);
        $categories = $model->getAllFrom('categories');
                
        $this->render('update_products', [
            'product' => $product,
            'categories' => $categories,
        ]);
    }
    
    public function saveProduct()
    {
        var_dump($_POST);
        if (isset($_POST['update_product']))
        {
            $product_name = $this->checkInput($_POST['product_name']);
            $category_id = $this->checkInput($_POST['category_id']);
            $product_id = $this->checkInput($_POST['product_id']);
            $product_price = $this->checkInput($_POST['product_price']);
            
            if (!empty($product_name) && ctype_digit($product_price))
            {
                $model = new PanelModel();
                $model->updateProduct($product_name, $category_id, $product_price, $product_id);
                
                header('Location: ' . URL . 'panel/products');
            } else {
                header('Location: ' . URL . 'panel/products');
            }
        }
    }
    
    public function showOrders()
    {
        $this->table = 'orders';
        
        $model = new PanelModel();
        $orders = $model->getAllForOrders();
        
        $this->render('table_orders', [
            'orders' => $orders,
        ]);
    }
    
    public function deleteOrder($order_id)
    {
        $this->table = 'orders';
        
        $model = new PanelModel();
        $model->deleteRow($order_id, $this->table);
        
        header('Location: ' . URL . 'panel/orders');
    }

    private function checkInput($var)
    {
        $var = trim($var);
        $var = stripslashes($var);
        $var = filter_var($var, FILTER_SANITIZE_STRING);

        return $var;
    }
}
