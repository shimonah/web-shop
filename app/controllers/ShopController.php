<?php
namespace app\controllers;

use app\models\ShopModel;
use app\core\Controller;

class ShopController extends Controller
{
	public $table;
    
        public function main()
	{
            $this->table = 'categories';
            
            $model = new ShopModel();
            $categories = $model->getAllFrom($this->table);
            
            $this->renderMain([
                'categories' => $categories,
            ]);
            
            
	}
        
        public function showCategory($category_id)
        {
            $model = new ShopModel();
            $products = $model->getProductsBy($category_id);
            
            $this->render('category', [
                'products' => $products,
                'category_id' => $category_id,
            ]);
            
        }
        
        public function showProduct($category_id, $product_id)
        {
            $model = new ShopModel();
            $product = $model->getProductBy($product_id);
            
            $this->render('product', [
                'product' => $product,
                'category_id' => $category_id,
            ]);
        }
        
        public function Order($product_id)
        {   
            $model = new ShopModel();
            $product = $model->getProductBy($product_id);
            
            $this->render('order', [
                'product' => $product,
            ]);
        }
        
        public function result()
        {
            $this->render('result');
            
        }
        
        public function addOrder($product_id)
        {
            if (isset($_POST['submit']))
            {
                $user_name = $this->checkInput($_POST['user_name']);
                $user_email = $this->checkInput($_POST['user_email']);
                
                if (filter_var($user_email, FILTER_VALIDATE_EMAIL))
                {
                    $model = new ShopModel();
                    $model->addOrder($product_id, $user_name);
                    header('Location: ' . URL . 'shop/result');
                } else {
                    header('Location: ' . URL . 'shop/order/' . $product_id);
                }
            }
        }
        
        private function checkInput($var)
        {
        $var = trim($var);
	$var = stripslashes($var);
	$var = filter_var($var, FILTER_SANITIZE_STRING);
        
	return $var;
        }   

}