<?php
namespace app\models;

use app\core\Model;
use PDO;

/**
 * Description of HomeModel
 *
 * @author alexey
 */
class ShopModel extends Model
{
   public function getAllFrom($table)
   {
        $sql = "SELECT * FROM $table";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
   }
   
   public function getProductsBy($category_id)
   {
       $sql = "SELECT products.* FROM products "
            . "INNER JOIN categories ON products.category_id = categories.id "
            . "WHERE category_id = :category_id";
       $query = $this->db->prepare($sql);
       $params = array(':category_id' => $category_id);
       $query->execute($params);
       
       return $query->fetchAll();
   }
   
   public function getProductBy($id)
   {
       $sql = "SELECT * FROM products WHERE id = :id";
       $query = $this->db->prepare($sql);
       $params = array(':id' => $id);
       $query->execute($params);
       
       return $query->fetch();
   }
   
   public function addOrder($product_id, $user_name)
   {
       $sql = "INSERT INTO orders (product_id, user_name) VALUES (:product_id, :user_name)";
       $query = $this->db->prepare($sql);
       $params = array(':product_id' => $product_id, ':user_name' => $user_name);
       $query->execute($params);
   }
}
