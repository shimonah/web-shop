<?php
namespace app\models;

use app\core\Model;
use PDO;

/**
 * Description of PanelModel
 *
 */
class PanelModel extends Model
{
    public function getAllFrom($table)
    {
        $sql = "SELECT * FROM $table";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }
    
    public function deleteRow($id, $table)
    {
        $sql = "DELETE FROM $table WHERE id = :id";
        $query = $this->db->prepare($sql);
        $params = array(':id' => $id);
        $query->execute($params);
    }
    
    public function addCategory($category_name)
    {
        $sql = "INSERT INTO categories (category_name) VALUES (:category_name)";
        $query = $this->db->prepare($sql);
        $params = array(':category_name' => $category_name);
        $query->execute($params);
    }
    
    public function getRow($id, $table)
    {
        $sql = "SELECT * FROM $table WHERE id = :id";
        $query = $this->db->prepare($sql);
        $params = array(':id' => $id);
        $query->execute($params);
        
        return $query->fetch();
    }
    
    public function updateCategory($category_name, $category_id)
    {
        $sql = "UPDATE categories SET category_name = :category_name WHERE id = :category_id";        
        $query = $this->db->prepare($sql);
        $params = array(
            
            ':category_name' => $category_name, 
            ':category_id' => $category_id);
        
        $query->execute($params);
    }
    
    public function getAllForProducts()
    {
        $sql = "SELECT categories.category_name, products.* "
             . "FROM products INNER JOIN categories "
             . "ON products.category_id = categories.id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }
    
    public function addProduct($product_name, $category_id, $product_price)
    {
        $sql = "INSERT INTO products (product_name, category_id, product_price) "
             . "VALUES (:product_name, :category_id, :product_price)";
        $query = $this->db->prepare($sql);
        $params = array(
            
            ':product_name' => $product_name, 
            ':category_id' => $category_id, 
            ':product_price' => $product_price);
        
        $query->execute($params);
    }
    
    public function updateProduct($product_name, $category_id, $product_price, $product_id)
    {
        $sql = "UPDATE products SET "
             . "product_name = :product_name, "
             . "category_id = :category_id, "
             . "product_price = :product_price "
             . "WHERE id = :product_id";
        
        $query = $this->db->prepare($sql);
        $params = array(
            
            ':product_name' => $product_name,
            ':category_id' => $category_id,
            ':product_price' => $product_price,
            ':product_id' => $product_id);
        
        $query->execute($params);
    }
    
    public function getAllForOrders()
    {
        $sql = "SELECT products.product_name, orders.* FROM orders "
             . "INNER JOIN products "
             . "ON orders.product_id = products.id";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetchAll();
    }
}
