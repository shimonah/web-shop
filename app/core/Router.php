<?php

namespace app\core;

use app\controllers\ShopController;

class Router
{
        public $routes;
        
        public function __construct()
        {
            $routespath = ROOT . 'app/config/routes.php';
            $this->routes = require_once($routespath);
        }
    
	private function getUri()
	{
		return trim($_SERVER['REQUEST_URI'], '/');
	}
	
	public function run()
	{
		$uri = $this->getUri();

		foreach ($this->routes as $uriPattern => $path)
		{

			if (preg_match("~$uriPattern~", $uri))
			{

				$internalroute = preg_replace("~$uriPattern~", $path, $uri);

				$segments = explode('/', $internalroute);
				
				$controllername = array_shift($segments);
				$controllername = ucfirst($controllername);
				$controllername = 'app\\controllers\\' . $controllername . 'Controller';
				
				$actionname = array_shift($segments);
				
				$parameters = $segments;

				$controller_obj = new $controllername;

				$result = call_user_func_array(array($controller_obj, $actionname), $parameters);

//				if ($result != NULL) {
					break;		
//				}
			}
		}
	}
}