<?php
namespace app\core;

use PDO;

class Model
{
	public $db = null;

	public function __construct()
	{
		$this->OpenDatabaseConnection();
	}

	public function OpenDatabaseConnection()
	{
		$options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ);
		$this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS, $options);
	}

}