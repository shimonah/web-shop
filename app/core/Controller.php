<?php
namespace app\core;

class Controller
{
	/**
	 * Description
	 * @param type $file 
	 * @param type|array $params 
	 * @return type
	 */
	public function render($file, $params = [])
	{
		$dir = $this->GetDirName();
		
		extract($params);
		require_once 'app/views/templates/header.php';
		require_once 'app/views/' . $dir . '/' . $file . '.php';
		require_once 'app/views/templates/footer.php';
	}
        
        public function renderMain($params = [])
        {
            $dir = 'shop';
            $file = 'main';
            
            extract($params);
            require_once 'app/views/templates/header.php';
            require_once 'app/views/' . $dir . '/' . $file . '.php';
            require_once 'app/views/templates/footer.php';
        }

	/**
	 * @return type
	 */
	private function GetDirName()
	{
		$line = trim($_SERVER['REQUEST_URI'], '/');
		$dir = strstr($line, '/', true);

		return $dir;
	}
}