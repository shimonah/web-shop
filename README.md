#Simple version of web shop.



###Руководство по разворачиванию сайта:



###Классы, которые используются:

PanelController.php - '../web-shop/app/controllers/PanelController.php'

ShopController.php - '../web-shop/app/controllers/ShopController.php'

PanelModel.php - '../web-shop/models/PanelModel.php'

ShopModel.php - '../web-shop/app/models/ShopModel.php'

###view-файлы:

'../web-shop/app/views/shop/'.
'../web-shop/app/views/panel/'.

###маршурты:

'../web-shop/config/routes.php'.


***


###Чтобы настроить соединение с базой данных, нужно зайти в файл 'config.php',который находиться в '../web-shop/app/config/config.php' и определить константы DB_NAME, DB_USER, DB_PASS.
###К приложению прилагается файл 'shop.sql' с описанием таблиц необходимых для работы.


***

###Конфигурации для apache-файла:

DocumentRoot "path/to/web-shop/"

<Directory "path/to/web-shop/">
    
    RewriteEngine on
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule . index.php

</Directory>

***

###Посмотреть сайт можно, написав в строке браузера имя, которые вы определили, как host для приложения.
