-- shop Database
-- 
CREATE TABLE categories(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    category_name VARCHAR(32)) ENGINE=INNODB;

CREATE TABLE products (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    product_name VARCHAR(64),
    category_id INT REFERENCES categories (id),
    product_price INT) ENGINE=INNODB;

CREATE TABLE orders (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    product_id INT REFERENCES products (id),
    user_name VARCHAR(32)) ENGINE=INNODB;
