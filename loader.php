<?php

function Loader($class)
{
	$arr = explode('\\', $class);
	$file = implode('/', $arr) . '.php';
	
	if (is_file($file)) {
		require_once $file;
	}
}